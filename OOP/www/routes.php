<?php
    return [
        '~^hello/(.*)$~' => [MyProject\Controllers\MainController::class, 'sayHello'],
        '~^$~' => [MyProject\Controllers\MainController::class, 'main'],
        '~^article/(\d)$~' => [MyProject\Controllers\ArticleController::class, 'view'],
        '~^article/(\d)/edit$~' => [MyProject\Controllers\ArticleController::class, 'edit'],
        '~^comment/(\d)$~' => [MyProject\Controllers\CommentsController::class, 'comment'],
    ]
?>