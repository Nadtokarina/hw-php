<?php
    namespace MyProject\Controllers;
    use MyProject\Models\Comments\Comment;
    use MyProject\Models\Articles\Article;
    use MyProject\Models\Users\User;
    use MyProject\View\View;

    class CommentsController{
        private $view;
        private $db;

        public function __construct(){
            $this->view = new View(__DIR__.'/../../../templates');
        }

        public function comment(int $comment){ 
            $comment = Comment::findAll();
            $reflector = new \ReflectionObject($comment);
            $properties = $reflector->getProperties();
            $propertiesName = [];
            foreach($properties as $property){
                $propertiesName[] = $property->getName(); 
            }

            
            if ($comment === []){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
            $this->view->renderHtml('comments/comment.php', ['comment' => $comment]);
        }
    }
?>