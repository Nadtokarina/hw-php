<?php
    namespace MyProject\Models\Comments;
    use MyProject\Models\Users\User;
    use MyProject\Models\Articles\Article;
    use MyProject\Models\ActiveRecordEntity;

    class Comment extends ActiveRecordEntity{
        protected $name;
        protected $comment;

        public static function getTableName(): string{
            return 'comments';
        }
        public function getText(){
            return $this->comment;
        }
        public function getName(){
            return $this->name;
        }
        public function setName(string $name){
            $this->name = $name;
        }
        public function setText(string $comment){
            $this->comment = $comment;
        }
    }
?>