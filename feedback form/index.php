<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Task 2</title>
</head>
<body>
    <header class="header">
		<a href="#"><img src="img/logo.png" width="200" height="70" style="height:50px; width:150px; margin-top:40px; margin-left:50px; margin-bottom: 20px" alt="logo "></a>
		<h1  style="text-align:center; margin-top: -50px; margin-bottom: 20px">Задание для самостоятельной работы «Hello, World!»</h1>
	</header>
    <main class="main">
        <form class="form" action="https://httpbin.org/post" method="POSt">
            <label class="label" for="name_id">Введите ваше имя: </label><input class="input" type="text" name="name" id="name_id" placeholder="Enter your name">
            <label for="email_id" class="label">Укажите ваш e-mail: </label><input class="input" type="email" name="emai" id="emai_id" placeholder="Enter your e-mail">
            <label for="purpose_id" class="label">Причина обращения: </label>
            <select class="input" name="purpose" id="purpose_id">
                <option value="Not selected" selected>Не выбрано</option>
                <option value="Сomplaint">Жалоба</option>
                <option value="Offer">Предложение</option>
                <option value="Gratitude">Благодарность</option>
            </select>
            <label for="text_id" class="label">Введите текст обращения: </label>
            <textarea class="input" name="text" id="text_id" cols="30" rows="10" placeholder="Enter your text"></textarea>
            <label for="sms_id" class="label"><input class="checkbox--sms" type="checkbox" name="sms" value="sms" id="sms_id">sms</label>
            <label for="e-mail_id" class="label"><input class="checkbox" type="checkbox" name="e-mail_checkbox" value="e-mail" id="e-mail_id">e-mail</label>
            <div class="buttons">
                <?php $href="form.php"; ?>
                <button class="button" type="submit">Send</button>
                <a href="form.php" class="link"> Go to the next page</a>
            </div>
        </form>
    </main>
	<footer class="footer">
		<p style="margin-top: 50px; text-align: center">Задание для самостоятельной работы.</p>
	</footer>
</body>
</html>