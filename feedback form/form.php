<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Answer</title>
</head>
<body>
    <header class="header">
		<a href="#"><img src="img/logo.png" alt="logo "style="height:50px; width:150px; margin-top:40px; margin-left:50px; margin-bottom: 20px"></a>
		<h1 style="text-align:center; margin-top: -50px; margin-bottom: 20px">Задание для самостоятельной работы «Hello, World!»</h1>
	</header>
    <main>
        <form class="form">
        <?php 
            $url = 'https://httpbin.org';
            $answer = get_headers($url);
        ?>
        <label for="answer_id">Результат работы функции get_headers: </label>
        <textarea name="answer" id="answer_id" cols="30" rows="20">
            <?php
                print_r($answer);
            ?>
        </textarea>
        </form>
    </main>
	<footer class="footer">
		<p class="text-footer">Задание для самостоятельной работы.</p>
	</footer>
</body>
</html>