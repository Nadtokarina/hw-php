<?php

    $login = filter_var(trim($_POST['login']), FILTER_SANITIZE_STRING);
    $name = filter_var(trim($_POST['name']), FILTER_SANITIZE_STRING);
    $email = filter_var(trim($_POST['email']), FILTER_SANITIZE_STRING);
    $password = filter_var(trim($_POST['password']), FILTER_SANITIZE_STRING);

    if (mb_strlen($login) < 5 || mb_strlen($login) > 90) {
        echo "Недопустимая длина логина";
        exit();
    }

    if (mb_strlen($password) < 2 || mb_strlen($password) > 8) {
        echo "Недопустимая длина пароля (от 2 до 8 символов)";
        exit();
    }

    $password = md5($password);

    $mysql = new mysqli('localhost','root','','register-bd');

    $mysql -> query("INSERT INTO `users` (`name`, `email`, `login`, `password`) VALUES ('$name', '$email', '$login', '$password')");


    $mysql -> close();

    header('Location: register.html');

?>