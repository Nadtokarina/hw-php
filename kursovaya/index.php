<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Аутентификация</title>
</head>
<body>

    <div class="container mt-4">
        <h2 class="container heading">Форма аутентификации</h2>
        <fieldset class="faildset">
            <form action="enter.php" method="post" class="form-enter">
                <label class="form-label" for="login">Введите логин: </label>
                <input class="form-control" type="text" name="login" id="login" placeholder="Ваш логин"><br>
                <label class="form-label" for="password">Введите пароль: </label>
                <input class="form-control" type="password" name="password" id="password" placeholder="Ваш пароль"><br>
                <button class="btn">Отправить</button>
            </form>
        </fieldset>
        <a href="register.html">Зарегистрироваться</a>
    </div>
</body>
</html>