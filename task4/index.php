<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Functions</title>
</head>
<body>
    <?php
        function solution($solution) {
            $string = explode(' ', $solution);
            $operand = $string[1];
            $unknown = array_search('x', $string);
            if ($unknown == 0 ) return calculator($string[4], $string[2], $operand);
            if ($unknown == 2) return calculator($string[4], $string[0], $operand);
        }
        function calculator($first, $second, $operand) {
            if ($operand == '+') return $first - $second;
            if ($operand == '-') return $first + $second;
            if ($operand == '*') return $first / $second;
            if ($operand == '/') return $first * $second;
        }

        echo solution('x / 8 = 6');

    ?>
</body>
</html>